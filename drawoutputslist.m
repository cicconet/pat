% This code is distributed under the MIT Licence.
% See notice at the end of this file.

function J = drawoutputslist(ignoredirections,I,x,y,m,a,hopsize,magfactor,showoriginal,showcoefficients)

if showoriginal
    J = imresize(0.25*I,magfactor);
else
    J = imresize(zeros(size(I)),magfactor);
end
J = repmat(J,[1 1 3]);
s = length(m);
k0 = floor(magfactor*hopsize/3);
if ignoredirections
    for j = 1:s
        for k = -k0:k0
            row = magfactor*x(j)+round(k*cos(a(j)));
            col = magfactor*y(j)+round(k*sin(a(j)));
            if row > 0 && col > 0 && row <= size(J,1) && col <= size(J,2)
                value = showcoefficients*m(j)+0.5*(1-showcoefficients);
                J(row,col,1) = value;
                J(row,col,2) = value;
                J(row,col,3) = 0;
            end
        end
    end
else
    for j = 1:s
        for k = 0:k0
            row = magfactor*x(j)+round(k*cos(a(j)));
            col = magfactor*y(j)+round(k*sin(a(j)));
            if row > 0 && col > 0 && row <= size(J,1) && col <= size(J,2)
                value = 0.5*(showcoefficients*m(j)+(1-showcoefficients));
                J(row,col,1) = value;
                J(row,col,2) = value;
                J(row,col,3) = 0;
            end
        end
        for ii = -1:1
            for jj = -1:1
                row = magfactor*x(j)+ii;
                col = magfactor*y(j)+jj;
                if row > 0 && col > 0 && row <= size(J,1) && col <= size(J,2)
                    J(row,col,1) = 1;
                    J(row,col,2) = 1;
                    J(row,col,3) = 0;
                end
            end
        end
    end
end

end

% Copyright (c) 2014 Marcelo Cicconet
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.