% This code is distributed under the MIT Licence.
% See notice at the end of this file.

I = imread('circuit.tif'); % rice.png, circuit.tif, circles.png, tire.tif, eight.tif
I = double(I)/255;

nangs = 16; % number of angles
stretch = 1; % stretch of morlet wavelet
scale = 2; % scale of morlet wavelet
hopsize = 5; % distance between centers of windows
halfwindowsize = 1; % half width of window
magthreshold = 0.05; % magnitude threshold for wavelet consideration
ignoredirections = 0; % 1: compute rods; 0: compute vectors

% J = waveletinimage(I,stretch,scale);
% imshow(J)
% return

[m,a,x,y] = coefficientslist(ignoredirections,I,nangs,stretch,scale,hopsize,halfwindowsize,magthreshold);
magfactor = 2;
showoriginal = 1;
showcoefficients = 1;
J = drawoutputslist(ignoredirections,I,x,y,m,a,hopsize,magfactor,showoriginal,showcoefficients);
imshow(J)

% Copyright (c) 2014 Marcelo Cicconet
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.