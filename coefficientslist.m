% This code is distributed under the MIT Licence.
% See notice at the end of this file.

function [m,a,x,y] = coefficientslist(ignoredirections,I,nangs,stretch,scale,hopsize,halfwindowsize,magthreshold)

[M,A,X,Y] = coefficientsmatrix(ignoredirections,I,nangs,stretch,scale,hopsize,halfwindowsize,magthreshold);
s = sum(sum(M > 0));
m = zeros(1,s);
a = zeros(1,s);
x = zeros(1,s);
y = zeros(1,s);
[nr,nc] = size(M);
index = 0;
for i = 1:nr
    for j = 1:nc
        if M(i,j) > 0
            index = index+1;
            m(index) = M(i,j);
            a(index) = A(i,j);
            x(index) = X(i,j);
            y(index) = Y(i,j);
        end
    end
end
m = m/max(m);

end

% Copyright (c) 2014 Marcelo Cicconet
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.